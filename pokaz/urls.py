#coding: utf-8
from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic import ListView, DetailView

#Dokładny opis: https://docs.djangoproject.com/en/1.4/ref/contrib/staticfiles/

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

    (r'^karty/', include('kartoteka.urls')),

    (r'^form_test/$', 'kartoteka.views.form_test'),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += staticfiles_urlpatterns()

