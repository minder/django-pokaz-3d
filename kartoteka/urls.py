#coding: utf-8
from django.conf.urls import patterns, include, url
from django.views.generic import ListView, DetailView

from kartoteka.models import Karta

urlpatterns = patterns('',

    url(r'^$', ListView.as_view(
        model=Karta,
    ), name='karta_lista'),

    url(r'^(?P<pk>\d+)/$', DetailView.as_view(
        model = Karta,
    ), name='karta_detal'),

    url(r'^(?P<pk>\d+)/edit/$', 'kartoteka.views.karta_edit', 
     name='karta_edycja'),
)
