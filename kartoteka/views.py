#coding: utf-8
#plik: views.py
from django.shortcuts import render_to_response
from kartoteka.models import Karta
from django.core.context_processors import csrf
from kartoteka.forms import KartaForm, PrzykladForm


def karta_edit(request, pk):
    if request.method == 'POST':
        #### analiza danych
        ### PRZEKIEROWANIE na stronę z podziękowaniami
        pass
    
    
    karta = Karta.objects.get(pk=pk)
    
    formularz = KartaForm(instance=karta)
    
    dane = {'formularz': formularz}
    dane.update(csrf(request))
    return render_to_response('karta_edit.html', dane)


def form_test(request):
    dane = {}
    if request.method == 'POST':
        # wypełniony formularz został odesłany do analizy
        formularz = PrzykladForm(request.POST)
        if formularz.is_valid():
            imie = formularz.cleaned_data['imie']
            nazwisko = formularz.cleaned_data['nazwisko']
            dane.update({'msg':u"Przesłano: %s %s" % (imie, nazwisko)})
            
    else:
        # strona została wczytana "na świeżo"
        dane.update({'msg':u"Proszę wypełnić formularz"})
        formularz = PrzykladForm()

    dane.update({'formularz':formularz})
    dane.update(csrf(request))
    return render_to_response('form_test.html', dane)
