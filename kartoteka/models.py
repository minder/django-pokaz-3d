#coding: utf-8
# South (django-south) - umożliwia migracje bazy danych, tzn. zmianę modelu bez kasowania tabeli.
from django.db import models
from django.core.urlresolvers import reverse


class Karta(models.Model):
    imie = models.CharField(max_length=20)
    nazwisko = models.CharField(max_length=50)
    data_ur = models.DateField(null=True, blank=True)
    telefon = models.CharField(max_length=16, blank=True)
    email = models.CharField(max_length=200, blank=True)
    fotka = models.ImageField(upload_to="fotki/", blank=True, null=True)
    
    def __unicode__(self):
        return "%s %s" % (self.imie, self.nazwisko)

    def get_url(self):
        return reverse('karta_detal', args=[self.id])
