#coding: utf-8
#plik: forms.py
from django import forms
from kartoteka.models import Karta


def check_pesel(number, check_date=False):
    """
    Checks if the date is OK and calculates a checksum with the provided algorithm.
    """
    if check_date:
        year = int(number[:2])
        month = int(number[2:4])
        day = int(number[4:6])

        if 40 > month >= 20:
            year += 2000
            month -= 20
        elif 60 > month >= 40:
            year += 2100
            month -= 40
        elif 80 > month >=60:
            year +=2200
            month -= 80
        elif month >= 80:
            year += 1800
            month -= 80
        else:
            year += 1900
        try:
            datetime.date(year, month, day)
        except ValueError as e:
            return False

    multiple_table = (1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 1)
    result = 0
    for i in xrange(len(number)):
        result += int(number[i]) * multiple_table[i]
    return result % 10 == 0



class KartaForm(forms.ModelForm):
    class Meta:
        model = Karta


class PESELField(forms.CharField):
    def validate(self, value):
        check_pesel(value)







class PrzykladForm(forms.Form):
    imie = forms.CharField(max_length=20, required=False)
    nazwisko = forms.CharField(max_length=20)
    pesel = forms.CharField(max_length=11, min_length=11)
    
    def clean_pesel(self):
        pesel = self.cleaned_data['pesel']
        if not check_pesel(pesel):
            raise forms.ValidationError(u"Nieprawidłowy PESEL")
    
